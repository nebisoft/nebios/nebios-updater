import os
import sys
from PyQt5.QtWidgets import *
import urllib3

class MainWindow(QMainWindow):

    def checkUpdateVer(self):
        link = "http://release.nebisoftware.com/NebiOS/latest-update/version-id.txt"
        http = urllib3.PoolManager()
        response = http.request('GET', link)
        ver = str(response.data.decode('utf-8'))
        print("Version "+ str(ver) + " released.\nVersion "+ sys.argv[1] + " is current.")
        equals = str(sys.argv[1]).__contains__(ver)
        print(equals)
        if (equals == False):
            self.updateTitle.setText("New release: NebiOS " + ver.replace("'",""))
            self.updateDesc.setText("Press ''Start Update'' to begin or close this window to cancel.\nUpdates are downloaded at background.\n\nLatest Release Notes:")
        else:
            self.updateTitle.setText("You're running the latest version of NebiOS! Yay!")
            self.updateDesc.setText("You can close this window.\n\nLatest Release Notes:")
            self.startUpdateBtn.setEnabled(False)

    def checkUpdateNotes(self):
        link = "http://release.nebisoftware.com/NebiOS/latest-update/changes.txt"
        http = urllib3.PoolManager()
        response = http.request('GET', link)
        notes = str(response.data.decode('utf-8'))
        self.updateNotes.setText(notes)

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setFixedWidth(640)
        self.setWindowTitle("NebiOS Updater")

        self.updateTitle = QLabel("Checking Updates...")
        fontTitle = self.updateTitle.font()
        fontTitle.setBold(True)
        self.updateTitle.setFont(fontTitle)

        self.updateDesc = QLabel("Close this window to cancel.")

        self.updateNotes = QTextEdit()
        self.updateNotes.setReadOnly(True)

        self.startUpdateBtn= QPushButton("Start Update")
        self.startUpdateBtn.clicked.connect(self.startUpdate)

        layout = QVBoxLayout()

        layout.addWidget(self.updateTitle)
        layout.addWidget(self.updateDesc)
        layout.addWidget(self.updateNotes)
        layout.addWidget(self.startUpdateBtn)

        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)
        self.checkUpdateVer()
        self.checkUpdateNotes()

        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def startUpdate(self):
        os.system("bash ./download.sh &")
        exit()


app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()
